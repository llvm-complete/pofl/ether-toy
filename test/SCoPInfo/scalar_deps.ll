; RUN: opt -polly-analyze-ir -polly-print-temp-scop-in-detail -analyze %s | not FileCheck %s
; XFAIL:*
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64"
target triple = "x86_64-linux-gnu"

define void @f(i64* nocapture %a, i64 %n) nounwind {
entry:
  %0 = icmp eq i64 %n, 0                          ; <i1> [#uses=1]
  br i1 %0, label %return, label %bb.nph

bb.nph:                                           ; preds = %entry
  %tmp4 = shl i64 %n, 1                           ; <i64> [#uses=1]
  br label %bb

bb:                                               ; preds = %bb, %bb.nph
  %i.03 = phi i64 [ 0, %bb.nph ], [ %1, %tmpbb ]     ; <i64> [#uses=3]
  %scevgep = getelementptr i64* %a, i64 %i.03     ; <i64*> [#uses=1]
  %tmp = mul i64 %i.03, 3                         ; <i64> [#uses=1]
  %tmp5 = add i64 %tmp4, %tmp                     ; <i64> [#uses=1]
  br label %tmpbb
  
tmpbb:
  store i64 %tmp5, i64* %scevgep, align 8
  %1 = add nsw i64 %i.03, 1                       ; <i64> [#uses=2]
  %exitcond = icmp eq i64 %1, %n                  ; <i1> [#uses=1]
  br i1 %exitcond, label %return, label %bb

return:                                           ; preds = %bb, %entry
  ret void
}

; CHECK: SCoP:
